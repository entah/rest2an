## Features

1. Load Object like-map as route entry and it's chain handlers and extend the re-useable capability of handlers inside express middleware.
2. All `Request` object filtered (you can set it's filter too) and all handlers have the same exact-copy of filtered request.
3. All handlers will receive context contained `req`, `deps`, `utils`, and `arg`.
4. All handlers is async function.
5. Inspect route available on your [express](https://github.com/expressjs/express) js app.
6. Auto test routes found on your Object like-map.
7. Test route by using Object like-map request.
8. Auto generated Swagger 2.0 basic definition based on auto test routes. (experimental)

## Limitation

1. For now, the auto test only work for `application/json`, `'application/x-www-form-urlencoded'`, and all string based MIME type
2. Swagger 2.0 cannot accept multi type array, so you may encounter error validation on auto generated swagger

## Instalation

### Manual 

You can add entry `rest2an: "https://gitlab.com/entah/rest2an#master"` inside `dependencies` section of `package.json`, and execute `npm install` or `yarn install`.

### Through `npm` or `yarn`

execute `yarn add https://gitlab.com/entah/rest2an#master` or `npm install https://gitlab.com/entah/rest2an#master` on you project's root directory

## Quick Use

Make sure you've installed `rest2an` and `express`

### auto test

create a file e.g: `index.js` on your project directory with below code as content.


```javascript
const {dev} = require('rest2an');
const {autoTest} = dev;

const echoHandlers = async ({req}) => ({res: {body: req}});

const routesConfig = {
    "get": {
        "/pink":{
            handlers: [
            // handlers can be string of function, function, or array contained path to source file and function name exported
                "async () => 'ponk'",
            ],
            // if you uncomment below entry, the route will not loaded in to your app
            // disabled: true,
            tests: [
                {req: {path: '/pink'}, res: {status: 200, body: 'ponk'}}, // res will assert the response
                // you can add more test here
            ]
        },
        "/echo":{
            // return the filtered Request object received by handler
            handlers: [
                echoHandlers
            ],
            tests: [
                {req: {path: '/echo'}} // if there is no res entry, the response will called by console.log
            ]
        },
        "/tbd-experiment": {
            // make this route not loaded
            disabled: true
        }
    },
    "post":{
        "/echo-body": {
            handlers: [
                "async () => 1",
                "async ({req}, arg) => arg + 2", //the second argument is the return value of first handler
                // if return value is an object with res entry or function wrap it, it will skip the rest of handlers,
                // below we make the process short-circuited and must return [3, req.body]
                "async ({req}, arg) => ({res: {body: [arg, req.body]}})",
                "async ({req}, arg) => ({res: {body: [arg + 100, req.body]}})"
            ],
            tests: [
                {req: {path: "/echo-body", payload: {foo: 'bar'}}, res: {body: [3, {foo: 'bar'}]}}
            ]
        }
    }
};

autoTest(routesConfig); // this will auto testing your route configurations


```

And execute `node index.js`. 

Your console may output like this: 
```javascript
server testing start on port: 10393
   test /pink succeded
{
  headers: {
    'x-powered-by': 'Express',
    'content-type': 'application/json; charset=utf-8',
    'content-length': '251',
    etag: 'W/"fb-wB2ef76sB13DJi7olbitnF9H5L4"',
    date: 'Wed, 19 Feb 2020 10:38:50 GMT',
    connection: 'close'
  },
  status: 200,
  path: '/echo',
  method: 'GET',
  body: {
    body: {},
    headers: { host: 'localhost:10393', connection: 'close' },
    url: '/echo',
    baseUrl: '',
    params: {},
    query: {},
    originalUrl: '/echo',
    method: 'GET',
    upgrade: false,
    aborted: false,
    httpVersion: '1.1',
    reqId: '78f11b4b09f050c481c83668f43ecb20'
  }
}
   test /echo-body succeded
```
