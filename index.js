"use strict";

const interceptor = require('./src/interceptor.js');
const utils = require('./src/utils.js');
const dev = require('./src/dev.js');

module.exports = {
    interceptor: interceptor,
    utils: utils,
    dev: dev
};
