"use strict";

const dev = require('../src/dev');
const test = require('ava');
const echoHandlers = async ({req}) => ({res: {body: req}});

const routesConfig = {
  "get": {
    "/pink":{
      handlers: [
	// handlers can be string of function, function, or array contained path to source file and function name exported
        "async () => 'ponk'",
      ],
      // if you uncomment below entry, the route will not loaded in to your app
      // disabled: true,
      tests: [
        {req: {path: '/pink'}, res: {status: 200, body: 'ponk'}}, // res will assert the response
	{name: "query params single value", req: {queries: {key: "value"}}, res: {status: 200, body: 'ponk'}}, // res will assert the response
	{req: {queries: {key: ["value1", "value2"]}}, res: {status: 200, body: (b) => b === 'ponk'}}, // res will assert the response
	{req: {queries: {key: ["value2"]}}, res: {status: 200, body: (b) => b === 'ponk'}}, // res will assert the response
        // you can add more test here
      ]
    },
    "/echo":{
      // return the filtered Request object received by handler
      handlers: [
        echoHandlers
      ],
      tests: [
        {req: {path: '/echo'}, res: {status: 200}} // if there is no res entry, the response will called by console.log
      ]
    },
    "/tbd-experiment": {
      // make this route not loaded
      disabled: true
    },
    "/some/:path/:param": {
      // request with path params
      handlers: [
	({req}) => req
      ],
      tests: [
	{ // check path params
	  req: {params: {path: "foo", param: "bar"}},
	  res: {status: 200, body: ({params: {path, param}}) => path === "foo" && param === "bar"}
	} 
      ]
    },
  },
  "post":{
    "/echo-body": {
      handlers: [
        "async () => 1",
        "async ({req}, arg) => arg + 2", //the second argument is the return value of first handler
        // if return value is an object with res entry or function wrap it, it will skip the rest of handlers,
        // below we make the process short-circuited and must return [3, req.body]
        "async ({req}, arg) => ({res: {body: [arg, req.body]}})",
        "async ({req}, arg) => ({res: {body: [arg + 100, req.body]}})"
      ],
      tests: [
        {req: {path: "/echo-body", payload: {foo: 'bar'}, headers: {"Content-Type": "application/json"}}, res: {body: [3, {foo: 'bar'}]}}
      ]
    }
  }
};


test("autoTest function", async t => {
  await dev.autoTest(routesConfig, null, t.done);
});

test("autoTest function, with error thrown by handler", async t => {
  const routes = {
    "post": {
      "/foo/bar": {
	handlers: [()=> {throw new Error("test");}],
	tests: [
	  {
	    name: "test error with payload form-urlencoded",
	    req: {
	      payload: {foo: "bar"},
	      headers: {'Content-Type': 'application/x-www-form-urlencoded'}},
	    res: {status: 200}
	  },
	  {
	    name: "test error with payload string",
	    req: {
	      payload: `{foo: "bar"}`,
	      headers: {}},
	    res: {headers: {"content-type": "plain/text"}}
	  },
	  {
	    name: "test error with payload object but not application/json",
	    req: {
	      payload: {},
	      headers: {}},
	    res: {headers: (h) => h["content-type"].includes("text/html")}
	  }
	]
      },
    },
  };

  await dev.autoTest(routes, t.done);
});

test("autoTestWithConfig function", async t => {
  await dev.autoTestWithConfig({config: routesConfig, callback: t.done, dumpOpts: {genSwagger: 'swagger.json'}});
});

test("inspectLoadedRoute function", async t => {
  const runable = dev.createExpressApp(routesConfig, {});
  t.deepEqual(
    dev.inspectLoadedRoute(runable),
    {
      get: [ '/echo', '/pink', '/some/:path/:param' ],
      post: [ '/echo-body' ]
    }
  );
});

test("describeRoutes function", t => {
  t.deepEqual(
    dev.describeRoutes(routesConfig),
    {
      get: [ '/echo', '/pink', '/some/:path/:param', '/tbd-experiment'],
      post: [ '/echo-body' ]
    }
  );
});

test("describeRoutes1 function", t => {
  t.deepEqual(
    dev.describeTableRoutes(routesConfig),
    [
      ['get', 'post'],
      ['/echo','/echo-body'],
      ['/pink', undefined],
      ['/some/:path/:param',undefined,],
      ['/tbd-experiment',undefined,],
    ]
  );
});

test("toCSVString function", t => {
  const described = dev.describeTableRoutes(routesConfig);
  t.is( dev.toCSVString(described),
`get,post
/echo,/echo-body
/pink,
/some/:path/:param,
/tbd-experiment,`);
});
