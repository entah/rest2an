const chai = require('chai');
const chaiHttp = require('chai-http');
const test = require('ava');
const dev = require('../src/dev');
const interceptor = require('../src/interceptor.js');

const { runableServer, server } = require('../test-resources/server/app.js');
chai.use(chaiHttp);

test.after('teardown test', () => server.close());

test('[POST] routes with stack of handlers where handlers on different file (modular handlers)', async t => {
  await chai.request(runableServer)
    .post('/reverse')
    .set("Content-Type", "text/plain")
    .send("foobar")
    .then(res => {
      t.is(res.text, 'raboof');
      t.is(res.status, 200);
    })
    .catch(err => { if (err) throw err; });
});

test('[POST] routes with string function', async t => {
  await chai.request(runableServer)
    .post('/echo')
    .set("Content-Type", "text/plain")
    .send("foobar")
    .then(res => {
      t.is(res.text, 'foobar');
      t.is(res.status, 200);
    })
    .catch(err => { if (err) throw err; });
});

test('[GET] routes with string function', async t => {
  await chai.request(runableServer)
    .get('/pink')
    .then(res => {
      t.is(res.text, 'ponk');
      t.is(res.status, 200);
    })
    .catch(err => { if (err) throw err; });
});

test('[GET] break the chain using object contains `res` key, skip the rest (modular handlers)', async t => {
  await chai.request(runableServer)
    .get('/break-with-object')
    .then(res => {
      t.is(res.text, 'Lorem Ipsum Dolor Sit Amet.');
      t.is(res.status, 200);
    })
    .catch(err => { if (err) throw err; });
});

test('[GET] break the chain if return value is function, skip the rest (handler is string function)', async t => {
  await chai.request(runableServer)
    .get('/break-with-fn')
    .then(res => {
      t.is(res.text, "chain handler break with arg 'foobar'");
      t.is(res.status, 200);
    })
    .catch(err => { if (err) throw err; });
});

test('[GET] error on handler will bubble up to the error handler middleware (use `Router` object)', async t => {
  await chai.request(runableServer)
    .get('/router/must-error')
    .then(res => {
      t.is(res.status, 501);
      t.is(res.text, 'internal server error');
    })
    .catch(err => { if (err) throw err; });
});


test('[GET] disable the route from config make the route not available (use `Router` object)', async t => {
  await chai.request(runableServer)
    .get('/router/not-found')
    .then(res => {
      t.is(res.status, 404);
      t.is(res.text, 'not found');
    })
    .catch(err => { if (err) throw err; });
});

test('[GET] deps evailable on handler context execution', async t => {
  await chai.request(runableServer)
    .get('/router/check-deps')
    .then(res => {
      t.is(res.status, 200);
      t.is(res.body.veddha, "Jakarta");
      t.is(res.body.hadi, "Bandung");
      t.is(res.body.fuja, "Bogor");
    })
    .catch(err => { if (err) throw err; });
});


test('[POST] req on handler context execution is immutable', async t => {
  await chai.request(runableServer)
    .post('/router/try-mutate-req')
    .set("Content-Type", "text/plain")
    .send("rest2an")
    .then(res => {
      t.is(res.text, 'rest2an');
      t.is(res.status, 200);
    })
    .catch(e => { if (e) throw e; });
});

test('[GET] try bare interceptor standalone', async t => {
  await chai.request(runableServer)
    .get('/interceptor/test')
    .then(res => {
      t.is(res.status, 200);
      t.is(res.text, "100");
    })
    .catch(e => { if (e) throw e; });
});

test('[PUT] response back path params', async t => {
  await chai.request(runableServer)
    .put('/try/foo/bar')
    .then(res => {
      t.deepEqual({ params: 'bar', path: 'foo' }, res.body);
      t.is(res.status, 200);
    })
    .catch(e => { if (e) throw e; });
});

test('[PUT] routes with unexpected value is ignored, lead to 404', async t => {
  await chai.request(runableServer)
    .put('/handler/is/unknown')
    .then(res => {
      t.is("not found", res.text);
      t.is(res.status, 404);
    })
    .catch(e => { if (e) throw e; });
});


test('[GET] use predefined utils deps', async t => {
  const route = {
    "get": {
      "/try/use/utils/deps": {
	handlers: [async ({utils: {genRandomHex}}) => ({res: {body: await genRandomHex(16)}})]
      },
      "/try/use/unavailable/deps": {
	handlers: [async ({utils: {genUUID}}) => ({res: {body: genUUID()}})]
      }
    }
  };

  const runable = dev.createExpressApp(route, {}, {utils_deps: ["genRandomHex"]});
  const server = runable.listen(null);

  await chai.request(runable)
    .get('/try/use/utils/deps')
    .then(res => {
      t.is(32, res.text.length);
      t.is(res.status, 200);
    })
    .catch(e => { if (e) throw e; });

  await chai.request(runable)
    .get("/try/use/unavailable/deps")
    .then(res => {
      t.is(res.status, 500);
    })
    .catch(e => { if (e) throw e; });

  server.close();
});


test('[GET] use custom utils deps object', async t => {

  const utilsDeps = {
    alwaysOne: () => 1,
    increment: a => ++a
  };
  
  const route = {
    "get": {
      "/try/use/custom/utils/deps": {
	handlers: [
	  async ({utils: {alwaysOne}}) => alwaysOne(),
	  async ({utils: {increment}}, arg) => ({res: {body: {result: increment(arg)}}})
	]
      },
    }
  };

  const runable = dev.createExpressApp(route, {}, {utils_deps: utilsDeps});
  const server = runable.listen(null);
  
  await chai.request(runable)
    .get("/try/use/custom/utils/deps")
    .then(res => {
      t.deepEqual({result: 2}, res.body);
      t.is(res.status, 200);
      
    })
    .catch(e => { if (e) throw e; });
  
  server.close();
});


test('[GET] use custom utils with primitive value ', async t => {
  
  const route = {
    "get": {
      "/try/use/custom/utils/primitive": {
	handlers: [
	  async ({utils}) => ({res: {body: {result: utils}, status: 201, headers: {"x-test-text": "test headers"}}})
	]
      },
    }
  };

  const runable = dev.createExpressApp(route, {}, {utils_deps: 1});
  const server = runable.listen(null);
  
  await chai.request(runable)
    .get("/try/use/custom/utils/primitive")
    .then(res => {
      t.deepEqual({result: {}}, res.body);
      t.is(res.status, 201);
      t.is("test headers", res.headers["x-test-text"]);
    })
    .catch(e => { if (e) throw e; });
  
  server.close();
});

test('[GET] test rest of handling mechanism ', async t => {
  
  const route = {
    "get": {
      "/function/with/falsey/return": {
	handlers: [
	  async () => () => false // will be not found
	]
      },
      "/return/falsey/value": {
	handlers: [
	  async () => false // will be not found
	]
      },
      "/function/with/object/return": {
	handlers: [
	  async () => () => ({foo: "bar"})
	]
      },
    }
  };

  const runable = dev.createExpressApp(route, {}, {utils_deps: null});
  const server = runable.listen(null);
  
  await chai.request(runable)
    .get("/function/with/falsey/return")
    .then(res => {
      t.is(res.status, 404);
    })
    .catch(e => { if (e) throw e; });

  await chai.request(runable)
    .get("/return/falsey/value")
    .then(res => {
      t.is(res.status, 404);
    })
    .catch(e => { if (e) throw e; });

  await chai.request(runable)
    .get("/function/with/object/return")
    .then(res => {
      t.is(res.status, 200);
      t.deepEqual(res.body, ({foo: "bar"}));
    })
    .catch(e => { if (e) throw e; });
  
  server.close();
});


test('test throw error when evaluate handlers', async t => {
  
  const route1 = {
    "get": {
      "/only/path/to/evaluate": {
	handlers: [
	  ['/test-resources/server/handlers.js'] // throw error when only path evaluated
	]
      }
    }
  };

  t.throws(interceptor.routesConfigParser(route1));

  const route2 = {
    "get": {
      "/var/not/found": {
	handlers: [
	  ['/test-resources/server/handlers.js', "foobar"]
	]
      }
    }
  };

  t.throws(interceptor.routesConfigParser(route2));
  
});
