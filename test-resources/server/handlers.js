"use strict";

const explodeString = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return {
    explodedString: req.body.split("")
  };
};

const reverseCharArray = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return arg.explodedString.reverse().reduce((init, next) => init + next);
};

const returnPathParam = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return req.params;
};

const breakChainResObj = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return {
    res: {
      body: "Lorem Ipsum Dolor Sit Amet."
    }
  };
};

module.exports = {
  explodeString: explodeString,
  reverseCharArray: reverseCharArray,
  returnPathParam: returnPathParam,
  breakChainResObj: breakChainResObj
};
