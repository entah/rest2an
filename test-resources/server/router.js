"use strict";

const {interceptor: intc} = require('../../index.js');
const {routesConfigParser} = intc;
const router = require('express').Router();
const fn = require('@entah/fn-js');


const deps = {
  personsCity: {
    veddha: "Jakarta",
    hadi: "Bandung",
    fuja: "Bogor"
  }
};

const mustError = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  throw new Error("this route must error");
};


const checkDepsOnHandler1 = async ({req, deps: {personsCity}, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return fn.selectKeys(personsCity, ["veddha"]);
};

const checkDepsOnHandler2 = async ({req, deps: {personsCity}, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return {...arg, ...fn.selectKeys(personsCity, ["hadi"])};
};

const checkDepsOnHandler3 = async ({req, deps: {personsCity}, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return {
    res: {
      body: {...arg, ...fn.selectKeys(personsCity, ["fuja"])}
    }};
};

const tryToMutateReq1 = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  req.body = 'foo';
};

const tryToMutateReq2 = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  req.body = 'bar';
};

const tryToMutateReq3 = async ({req, deps, utilsDeps}, arg) => { // eslint-disable-line no-unused-vars
  return req.body;
};


const routesConfig = {
  "get": {
    "/must-error": {
      handlers: [
	mustError,
	['/test-resources/server/handlers.js', "breakChainResObj"],
	['/test-resources/server/handlers.js', "explodeString"],
	['/test-resources/server/handlers.js', "reverseCharArray"]
      ]
    },
    "/not-found": {
      handlers: [
	"() => { return {step: 'foobar'}; }",
	"() => { return {step: 2}}",
	"() => { return {res: {body: 'lorem ipsum dolor sit amet'}}}"
      ],
      disabled: true
    },
    "/check-deps": {
      handlers: [
	checkDepsOnHandler1,
	checkDepsOnHandler2,
	checkDepsOnHandler3,
      ]
    },
  },
  "post": {
    "/try-mutate-req": {
      handlers: [
	tryToMutateReq1,
	tryToMutateReq2,
	tryToMutateReq3
      ]
    },
  },
};

const mountableRoutes = routesConfigParser(routesConfig, deps, {});

module.exports = mountableRoutes(router);
