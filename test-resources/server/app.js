"use strict";

const {interceptor: intc} = require('../../index.js');
const {interceptor, routesConfigParser} = intc;
const express = require('express');
const router = require('./router.js');
const runableServer = express();


runableServer.use(express.json());
runableServer.use(express.text());
runableServer.use(express.urlencoded({extended: true}));

// const utilsDeps = {
//     alwaysZero: () => 1,
//     increment: a => ++a
// };

// const utils_keys = ["stringToBase64", "genUUID"];

const breakChainWithFN = async ({req, deps, utilsDeps}, arg) => {  // eslint-disable-line no-unused-vars
  return () => {
    return {
      res: {body: `chain handler break with arg '${arg.step}'`}
    };
  };
};

const routesConfig = {
  "post": {
    "/reverse": {
      handlers: [
	['/test-resources/server/handlers.js', "explodeString"],
	['/test-resources/server/handlers.js', "reverseCharArray"]
      ]
    }, 
    "/echo": {
      handlers: [ "async ({req}) => req.body" ]
    }
  },
  "get": {
    "/pink": {
      handlers: [ "async () => 'ponk'" ]
    },
    "/break-with-object": {
      handlers: [
	['/test-resources/server/handlers.js', "breakChainResObj"],
	['/test-resources/server/handlers.js', "explodeString"],
	['/test-resources/server/handlers.js', "reverseCharArray"]
      ]
    },
    "/break-with-fn": {
      handlers: [
	"() => { return {step: 'foobar'}; }",
	breakChainWithFN,
	"() => { return {step: 2}}",
	"() => { return {res: {body: 'lorem ipsum dolor sit amet'}}}"
      ]
    }
  },
  "put": {
    "/try/:path/:params": {
      handlers: [
	['/test-resources/server/handlers.js', "returnPathParam"]
      ]
    },
    "/handler/is/unknown": {	// this will be ignored since handler is unexpected value
      handlers: [1]
    }
  }
};


const intStandalone1 = async () => {
  return 10;
};

const intStandalone2 = async (deps, arg) => {
  return JSON.stringify(arg * 10);
};

routesConfigParser(routesConfig, {}, {req_keys: null, utils_deps: null})(runableServer);

runableServer.use('/router', router);

runableServer.get('/interceptor/test', interceptor([intStandalone1, intStandalone2]));

// 404 status handler
runableServer.use((req, res) => res.status(404).send('not found').end());

// error handler
runableServer.use((err, req, res, _next)=> { // eslint-disable-line no-unused-vars
  res.status(501).send('internal server error').end();
});

const server = runableServer.listen(null, () => console.log('server started'));

module.exports = {
  runableServer: runableServer,
  server: server
};
