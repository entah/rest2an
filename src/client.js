/**
 * @module client
 */

"use strict";

const fn = require('@entah/fn-js');
const http = require('http');
const qs = require('querystring');

/**
 * Send request return `Promise`.
 * This function wrap `http.request` from `Node.js`.
 * Return value by applying `assertion` into response object.
 * 
 * @function request
 * @param {Object} req - Like `Node.js` request options, but accept path parameters and query parameters
 * @param {Object} req.queries - query parameters, accept single value or multiple values encoded by `querystring`
 * @param {Object} req.params - path parameters, key is replacer for `:path` found in `req.path`
 * @param {Object} req.headers - headers to send within request
 * @param {string} req.path - path to send withing request
 * @param {string} req.host - host of the target request
 * @param {integer} [req.port] - optional host port
 * @param {integer} [req.timeout] - optional client request timeout in ms
 * @param {Object} [respOpts] - option to handle response
 * @param {string} [respOpts.resEncoding] - option to set encoding of response
 * @param {(string | Array | WritableStream)} [respOpts.resReader] - handling response body
 * @param {Function} [assertion=`@entah/fn-js#identity`] - function to assert response
 * @return {Promise<Object>} - result by applying `assertion` with response object
 */
const request = (req, {resEncoding, resReader}, assertion = fn.identity) => {
  return new Promise((resolve, reject) => {

    const {path, queries, params = {}}  = req;
    let parsedPath = fn.reduce((init, [k, v]) => init.replace(`:${k}`, v), params, path);
    const encoded = qs.stringify(queries);
    parsedPath = encoded ? parsedPath + "?" + encoded : parsedPath;
    
    const client = http.request(fn.assoc(req, "path", parsedPath));

    client.on('response', res => {

      if (resEncoding) res.setEncoding(resEncoding);

      const resp = fetchResponse(
	res,
	(status, headers, body) => ({ headers: headers, status: status, body: body, path: parsedPath}),
	resReader
      );
      resp.then((res) => resolve(assertion(res))).catch(reject);
    });

    client.on('error', reject);
    client.on('timeout', reject);
    client.on('abort', reject);

    if (req.payload) client.write(req.payload, "utf8");

    client.end();
  });
};


// const assumeReadStream = (thing) => fn.isObj(thing, true) ? fn.isFn(thing.read) : false;

/**
 * assume if `thing` is implement `WritableStream`.
 * 
 * @private
 * @function assumeWriteStream
 * @param {any} thing - arg to assume
 * @return {boolean} - true if `thing` implement `WritableStream`.
 */
const assumeWriteStream = (thing) => fn.isObj(thing, true) ? fn.isFn(thing.write) && fn.isFn(thing.end) : false ;


/**
 * assume if `thing` is implement `WritableStream`.
 * 
 * @private
 * @function fetchResponse
 * @param {Object} resObj - `Node.js` `http.IncomingMessage`
 * @param {function} respFn - function to call when `end` event happen in `resObj`
 * @param {(string | Array | WritableStream)} reader - handling response body
 * @return {Promise<Object>} - return `Promise`
 */
const fetchResponse = (resObj, respFn, reader) => {

  return new Promise((resolve, reject) => {
    if (assumeWriteStream(reader)) {
      resObj.on("readable", resObj.pipe(reader));

    } if (fn.isArr(reader)) {
      resObj.on('data', chunk => reader.push(chunk));

    } if (fn.isStr(reader)) {
      resObj.on('data', chunk => reader += chunk.toString());

    } else {
      resObj.on('data', chunk => chunk);
    }

    resObj.on('end', () => resolve(respFn(resObj.statusCode, resObj.headers, reader)));
    resObj.on('error', reject);
  });
};

module.exports.request = request;
module.exports.fetchResponse = fetchResponse;
