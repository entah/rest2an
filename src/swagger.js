/**
 * @module swagger
 */

"use strict";

const fn = require('@entah/fn-js');

const pathsToSwagger = (apiDef, paths) => {
  const byPaths = fn.groupBy(({req: {rawPath}}) => rawPath, paths);
  const parsedPaths = fn.reduce((init, [k, v]) => {

    const byMethods = fn.reduce((methods, route) => {

      const {req: {method, desc = '', rawPath}} = route;
      const parameters = genParameters(v);
      const {consumes, produces} = genConsumeProduce(v);
      const responses = aggregateResponse(v);
      return fn.assoc(methods, method.toLowerCase(), {
	tags: [],
	summary: '',
	description: desc,
	operationId: `[${method}] ${rawPath}`,
	consumes: consumes,
	produces: produces,
	parameters: parameters,
	responses: responses,
      });
    }, v, {});

    if (k.includes("/:")) {
      const params = fn.getIn(v, [0, "req", "params"]);
      k = fn.reduce((init, [k,]) => init.replace(`:${k}`, "{"+k+"}"), params, k);
    }
    
    return fn.assoc(init, k, byMethods);
  }, byPaths, {});

  return fn.assoc(apiDef, "paths", parsedPaths);
};

const reportQueries =  (paths) => {
  const calculated = fn.reduce((init, {req: {queries}}) => {
    if (!queries) {
      init.count = init.count + 1;
      return init;
    }

    return fn.reduce((i, [k, v]) => {
      i.count = i.count + 1;

      if (i[k]) {
	i[k].freq += 1;
	i[k].multiple = i[k].multiple ? i[k].multiple : fn.isArr(v);
	i[k].type = i[k].multiple ? typeof (fn.first(v)) : typeof v;
	return i;
      }

      const type = fn.isArr(v) ? typeof (fn.first(v)) : typeof v;
      return fn.assocIn(i, [k], {freq: 1, multiple: fn.isArr(v), type: type});
      
    }, fn.iterable(queries), init);
  }, paths, { count: 0 });

  const count = calculated.count;
  const qrs = fn.dropKeys(calculated, ["count"]);

  return fn.map(([k, {freq, multiple, type}]) => {
    const result = {
      "in": 'query',
      "name": k,
      "description": '',
      "required": freq == count,
      "type": multiple ? 'array' : type,
    };
    
    if (multiple) {
      result.items = {"type": type};
      result.collectionFormat = 'multi';
    }
    
    return result;
  }, qrs);
};

const explodeArray = (arr) => {
  const items = fn.map((e) => {
    if (fn.isObj(e, true)) return explodeObject(e);
    if (fn.isArr(e)) return explodeArray(e);
    return {
      type: typeof e
    };
  }, arr);
  const deduped = fn.dedupe(items);
  
  // if (deduped.length > 0) {
  return { type: "array", items: deduped[0] };
  // }

  // return { type: "array", items: {} };
};

const emptiness = (thing) => {
  if (!thing) return true;
  if (fn.isObj(thing, true) || fn.isArr(thing) || fn.isStr(thing)) return thing.length < 1;
  return thing && false;
};

const explodeObject = (obj) => {
  return {
    type: "object",
    properties: fn.reduce((init, [k,v]) => {
      if (emptiness(v)) return init;
      if (fn.isObj(v,true)) return fn.assoc(init, k, explodeObject(v));
      if (fn.isArr(v)) return fn.assoc(init, k, explodeArray(v));
      return fn.assoc(init, k, {type: typeof v});
    }, obj, {})
  };
};

const templateBodyParam = (schema) => {
  return {
    "in": 'body',
    "name": 'body',
    "description": '',
    "schema": schema,
  };
};

const setToArray = (set) => {
  const res = [];
  for (const e of set.entries()) {
    res.push(e[0]);
  }
  return res;
};

const genConsumeProduce = (paths) => {
  const collected =  fn.reduce((init, {req, resp}) => {
    const reqHeaders = req.headers || {};
    const respHeaders = resp.headers;
    const consume = reqHeaders['Content-Type'] || reqHeaders['content-type'];
    const produce = respHeaders['Content-Type'] || respHeaders['content-type'];
    if (consume) init.consumes.add(consume);
    if (produce) init.produces.add(produce.split(";")[0]);
    return init;
  }, paths, {consumes: new Set(), produces: new Set()});
  return {
    consumes: setToArray(collected.consumes),
    produces: setToArray(collected.produces),
  };
};

const aggregateResponse = (paths) => {
  // 1. collect response body and indexing by status code
  // 2. try to merge response body if response body are object
  // 3. if one of indexed response body has different type, choose the long one if object or array ? maybe we can do deep merge
  // 4. do parse to the selected body
  const indexed = fn.reduce((init, {resp: {status, body}}) => {
    return fn.updateIn(init, [status], (o, n) => {
      if (!o) return n;
      // if (!n) return o;
      if (!fn.isObj(n, true) && !(fn.isArr(n))) return o;
      return fn.deepMerge(o, n);
    }, body);
  }, paths, {});
  
  return fn.reduce((init, [k, v]) => {
    const exploded = explodeBody(v);

    if (exploded) return fn.assoc(init, k, {description: `status code ${k}`, schema: exploded});
    
    return fn.assoc(init, k, {description: `status code ${k}`});
  }, indexed, {});
};

const explodeBody = (body) => {
  if (!body) return null;
  
  if (fn.isStr(body)) {
    return {"type": 'string'};
  }

  if (fn.isObj(body, true)) return explodeObject(body);
  
  return explodeArray(body);
};

const reportBodies = (paths) => {
  // 1. collect all body request
  // 2. try to merge body request if body request are object
  // 3. when collected body request has different type, choose the first encounter
  // 4. do parse to the selected body

  const collected = fn.reduce((init, {req: {rawPayload}}) => {
    if (!rawPayload) return init;
    return [...init, rawPayload];
  }, paths, []);

  if (collected.length < 1) return collected;
  if (collected.length < 2) return [templateBodyParam(explodeBody(collected))];

  const merged = fn.apply(fn.deepMerge, collected);
  const exploded  = explodeBody(merged);
  return [templateBodyParam(exploded)];
};

const genParameters = (paths) => {
  const params = fn.map(([k, v]) => {
    return {
      "in": 'path',
      "name": k,
      "description": '',
      "required": true,
      "type": typeof v
    };
  },fn.first(paths).req.params || {});
  
  return [...params, ...reportQueries(paths), ...reportBodies(paths)];
};

module.exports.pathsToSwagger = pathsToSwagger;
