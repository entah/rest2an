/**
 * @module interceptor
 */

"use strict";

const fn = require('@entah/fn-js');
const utils = require('./utils.js');
const process = require('process');

/**
 * an array of keys to extract from `Request` object
 *
 * @private
 * @const
 */
const reqKeysToExtract = [
  'aborted',     'baseUrl',
  'body',        'cookies',
  'headers',     'hostname',
  'httpVersion', 'ip',
  'ips',         'method',
  'originalUrl', 'params',
  'path',        'protocol',
  'query',       'signedCookies',
  'stale',       'subDomains',
  'upgrade',     'url',
  'xhr'
];


/**
 * give array of keys return function with 1 arg (`Request` object) that would return a new object extracted by keys
 *
 * @private
 * @function extractReqX
 * @param {Array} [keysToExtract] - array of keys to extract the object
 * @return {Function} function with 1 arg
 */
const extractReqX = (keysToExtract) => req => {
  return fn.clone(fn.selectKeys(req, keysToExtract || reqKeysToExtract));
};


/**
 * evaluate path config contains stack of handlers by file path and the name of var inside an array or string of functions
 * return evaluated stack of handler
 *
 * @private
 * @function evaluatePathConfig
 * @params {Object} param
 * @param {Array} - param.handlers
 * @param {Boolean} - param.disabled
 * @return {(Array | Null)} return array of function handler or null if disabled of handler array was empty
 */
const evaluatePathConfig = ({handlers, disabled}) => {
  if (!handlers) return null;
  if (!disabled && (fn.count(handlers) > 0)) {
    const handlerList = fn.reduce((init, handler) => {
      if (fn.isStr(handler)) return [...init, eval(handler)];

      if (fn.isArr(handler)){
	if (handler.length < 2) throw new Error('must contains 2 items, first is path, second is function name');
	const [path, fnName] = handler;
	const ns = require(process.cwd() + path);
	const varFn = fn.get_(ns, fnName);
	if (!varFn) throw new Error(`Function '${fnName}' on '${ns}' not found`);
	return [...init, varFn];
      }

      if (fn.isFn(handler)) return [...init, handler];

      return init;
    }, handlers, []);
    if (handlerList.length < 1) return null;
    return handlerList;
  }

  return null;
};


/**
 * generate utils deps for added into context. if arg not supplied will add utils function on `../utils.js`.
 * if arg is an array with keys, return object of utils function on '../utils.js' filtered by keys.
 * if arg is a object, return object by calling spread operator {...arg}.
 * else will return the arg
 *
 * @private
 * @function generateUtilsDeps
 * @param {any} arg - the arg to generate utils deps
 * @return {any} returned result
 */
const generateUtilsDeps = arg => {
  if (!arg) return fn.clone(utils);
  if (fn.isArr(arg)) return fn.selectKeys(utils, arg);
  if (fn.isObj(arg)) return fn.clone(arg);
  return {};
};


/**
 * routesConfigParser.
 * Return function to parse and adding method + routes found in config to the app.
 * The returned function receive 1 arg. the arg could be an `Application` instance or `Router` instance of expressjs.
 *
 * @param {Object} config - an object describing the method, path, and stack of handlers
 * @param {any} [deps] - dependencies that would be available on handler's execution
 * @param {Object} [miscOptions] - options to enable `req_keys` and `utils_deps`
 * @param {(Object | Array | null)}  [miscOptions.utils_deps] - options to passed utils deps to handler's execution 
 * @param {(Array | null)}  [miscOptions.req_keys] - options that would enable specific keys from request object on handler's execution
 * @return {Function} function with arg of `Application` or `Router` of expressjs
 */
const routesConfigParser = (config, deps, miscOptions) => app => {
  const {utils_deps, req_keys} = miscOptions || {};
  const utilsDeps = generateUtilsDeps(utils_deps);
  fn.iterable(config).forEach(([method, routes]) => {
    fn.iterable(routes).forEach(([path, routeConfig]) => {
      const handlers = evaluatePathConfig(routeConfig);
      if (handlers) fn.execMethod(app, method)(path, interceptor(handlers, deps, utilsDeps, req_keys));
    });
  });
  return app;
};


/**
 * give deps, utils, and req,  return function that receive handler as it argument.
 * the returned function will return async function that will receive arg and execute the handler with supplied deps, utils, req and arg.
 * will skip the execution of handler if arg is function, and will wrap the result into function if result containe `res` key.
 *
 * @private
 * @function wrapHandler
 * @param {any} [deps] - dependencies that would be available on handler's execution
 * @param {any} [utils] - utils that would be available on handler's execution
 * @param {Object} req - request object that would be available on handler's execution
 * return {Function} function that would receive handler function as it only arg
 */
const wrapHandler = (deps, utils, req) => handler => async arg => {
  if (fn.isFn(arg)) return arg;
  const result = await handler({req: fn.clone(req), deps: fn.clone(deps), utils: fn.clone(utils)}, arg);
  if (fn.isObj(result)) return result.res ? fn.constantly(result) : result;
  return result;
};


/**
 * like `wrapHandler` function but only for the first handler in the stack
 * 
 * @private
 * @function wrapFirstHandler
 * @param {any} [deps] - dependencies that would be available on handler's execution
 * @param {any} [utils] - utils that would be available on handler's execution
 * return {Function} function that would receive handler function as it only arg
 */
const wrapFirstHandler = (deps, utils) => handler => async req => {
  const result = await handler({req: fn.clone(req), deps: fn.clone(deps), utils: fn.clone(utils)});
  if (fn.isObj(result)) return result.res ? fn.constantly(result) : result;
  return result;
};


/**
 * give Object contain headers, (and/or) status and `Response` object, 
 * return `Response` object contain headers appended via `Response.append()` and status via `Response.status()` method 
 * 
 * @private
 * @function appendHeadersAndStatus
 * @param {Object} [resMap={}] - Object contain headers and/or status entry 
 * @param {Object} resObj - `Response` object
 * @return {Object} `Response` object contain headers appended via `Response.append()` method
 */

const appendHeadersAndStatus = (resMap, resObj) => {
  if (resMap.headers && fn.isObj(resMap.headers)){
    for (const header in resMap.headers) {
      resObj.append(header, resMap.headers[header]);
    }
  }
  if (resMap.status) resObj.status(resMap.status);
  return resObj;
};


/**
 * interceptor.
 *
 * Give `handlers` return `expressjs` async function middleware signature.
 * `deps`, `utilsDeps`, and `req_keys` is optional. 
 * `handlers` will executed all with `req`, `deps`, `utilDeps` and maybe `arg` from previous handler.
 * 
 * @param {Array} handlers - an array of handlers to execute
 * @param {any} [deps] - dependencies that would be available on handler's execution
 * @param {any} [utilsDeps] - utils that would be available on handler's execution
 * @param {Array} [req_keys] - entry from `Request` object to extract
 * @return {Function} expressjs async function middleware signature like `async (req, res, next) => {}`
 */
const interceptor = (handlers, deps, utilsDeps, req_keys) => async function interceptor(request, response, next) {
  const reqId = await utils.genRandomHex(6);
  const requestExtractor = extractReqX(req_keys);
  const reqToPass = {...requestExtractor(request), reqId: reqId};
  const restHandler = wrapHandler(deps, utilsDeps, reqToPass);
  const firstHandler = wrapFirstHandler(deps, utilsDeps);
  const handlersStack = fn.map((handler, index) => index == 0 ? firstHandler(handler) : restHandler(handler), handlers);
  try {
    const result = await (fn.apply(fn._compose, handlersStack)(reqToPass));
    
    if (!result) {
      // if result isn't thruthy value call next()
      console.warn(`handlers return falsey value`);
      next();
      return;
    }
    
    if (fn.isFn(result)) {
      // if result is a function, evaluate it without arg
      const maybeResult = result();
      
      if (fn.getIn(maybeResult, ['res', 'body'])) {
	appendHeadersAndStatus(maybeResult.res, response).send(maybeResult.res.body).end();
	return;
      }

      // we send the `maybeResult` as response
      if (maybeResult) {
	response.send(maybeResult).end();
	return;
      }
      
      console.warn(`handlers return falsey value`);
      next();
      return;
    }
    
    // if (fn.getIn(result, ["res", "body"])) {
    //   //if result is an object and have body inside res, attach headers (if found) and send the body
    //   appendHeadersAndStatus(result.res, response).send(result.res.body).end();
    //   return;
    // }

    // last resort, we send the `result`
    response.send(result).end();
  } catch (ex) {
    next(ex);
  }
};


module.exports.interceptor = interceptor;
module.exports.routesConfigParser = routesConfigParser;
