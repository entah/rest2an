/**
 * @module utils
 */

"use strict";

const fs = require('fs');
const fn = require('@entah/fn-js');
const crypto = require('crypto');
const { Buffer } = require('buffer');


/**
 * decode base64 string
 * 
 * @param {String} string - base64 string encoded
 * @return {String} decoded string
 * @throws throw error if arg isn't string type
 */
const base64Parser = string => {
  if (!fn.isStr(string)) throw new Error('arg must string type');
  return Buffer.from(string,'base64').toString('utf8');
};


/**
 * encode string to base64 string encoded
 *
 * @param {String} string - string to encode
 * @return {String} base64 string encoded
 * @throws throw error if arg isn't string type
 */
const stringToBase64 = string => {
  if (!fn.isStr(string)) throw new Error('arg must string type');
  return Buffer.from(string, 'utf8').toString('base64');
};


/**
 * composeable function to read a file. wrap `fs.readFileSync`. the content converted to string by `Buffer.from`
 *
 * @function toStringX
 * @param {String} path - path to a file 
 * @return {String} string content of a file
 * @throws all error as `fs.readFileSync` and `Buffer.from` will thrown
 */
const toStringX = fn.compose(fs.readFileSync, buffer => Buffer.from(buffer).toString('utf8'));


/**
 * composeable function to read a JSON file
 *
 * @function readConfigX
 * @param {String} path - path to a file 
 * @return {any} result of `JSON.parse` on string
 * @throws all error as `fs.readFileSync`, `Buffer.from`, and `JSON.parse` will thrown
 */
const readConfigX = fn.compose(toStringX, JSON.parse);


/**
 * return promise from calling `crypto.randomBytes` with 16 length then converted to hex string
 *
 * @private
 * @function PromisableRandomHex
 * @param {Number} n - number of random hex
 * @return {Promise<string>}
 */
const PromisableRandomHex = n => {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(n, (err, buff) => {
      if (err) reject(err);
      resolve(buff.toString('hex'));
    });
  });
};


/**
 * return UUID like random generated string.
 *
 * @async
 * @function genUUID
 * @param {Boolean} flatten - if true will output string without hypen '-' as UUID will do.
 * @return {Promise<string>} UUID generated
 */
const genUUID = async flatten => {
  const generated = await PromisableRandomHex(16);
  if (flatten) {
    return generated;
  } else {
    const regex = new RegExp('^(.{8})(.{4})(.{4})(.{4})(.{12})$');
    return fn.rest(generated.match(regex)).join('-');
  }
};


/**
 * return random hex string.
 *
 * @async
 * @function genRandomHex
 * @param {Number} n - number of byte to generate
 * @return {Promise<string>} random hex string
 */
const genRandomHex = async n => {
  return await PromisableRandomHex(n);
};

module.exports.toStringX = toStringX;
module.exports.readConfigX = readConfigX;
module.exports.genUUID = genUUID;
module.exports.base64Parser = base64Parser;
module.exports.stringToBase64 = stringToBase64;
module.exports.genRandomHex = genRandomHex;
